# Acoustics and comms

## Acoustics

- Costs
  - Hydrophone cost / other needed equipment
  - Time cost (complexity)
  - power cost (for the bots)
  - spatial costs

### Communications

- Modulations
- Meshing
- Ranges / interference
- IDs (Physical or protocol)
- throughput

### Detection (Localization)

- Resolution
- occlusion / multi-path
- Identification (does it need a separate system)
- Hybrid with cameras

## Radio Frequency

- Antenna types
- ranges
- Can we fit the antenna

## Optical

### Communications

- LiFi
- simplified transmission (blink codes?)

### Detection

- inside-out vs outside in
- camera based seems like the only way to me for outside in
