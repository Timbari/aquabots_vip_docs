# Evaluation of Sensing Modalities and Methods

## Optical

Optical sensors have a huge advantage over other parts of the electro-magnetic spectrum in that the absorption is reasonable. The issue is that there is alot of interference in the optical band and the detection and transmission power is high.

### Camera

Camera based tracking is interesting as it spans the continum form low information to high information approches. The main issues facing cameras is as follows 

- Turbidity  the opacity of the water will increase with striing of contaminents and of bubbles in the water.
- Distortion of based on the refraction caused by the media changes and abreations in the glass as well as distrubances in the water.
- occlusion the optical spectrum has no transmission through the bot so on persepctive is not suffcient to track bots in the whole tank
- Depth perception  a camera does not implicly knoe depth so the addition of more camera prespecive is neccissar or stere pairs etc.

#### Fiducials

This is a high information approach which use a tag or marcher that can be decoded and an id assigned additionally the position can be found from the known scale and shape of the fiducial. Issues with fiducials are as follows.

- The need a relatively clear image to extract a tag this means that most of the challenges associated with cameras are amplified.
- the binary if the tag is not detected there is no information.
- 

#### Marker

A marker is a part of the bot that can be easily detected and tracked frame to frame then using multiple synced cameras the 3D pose can be extracted. The detection of this is much lower information than fiducials and can be scaled to a larger number of markers some of the challenges are as follows

- Data association which markers belong to wich bots.
- 3D require at least 2 preferably 3 cameras to see it at once this makes the occlusion problem harder.
- Might need to vary the markers reflectivity at different wave lengths. (we need some way to id the groups)

#### Color Tracking

Color tracking leverages the entire bot as a marker this is simple but requires some work to seperate bots that are close together

### Photo detectors

Photo detectors could be used in a timing based pulse sequence to give location information howeever this is usually inside out tracking meaing that the bot must track its own position and send it back to the topside.


#### Light houses

## Acuostic
