# Design Notebook

## 1/10

- We held a prep meeting to get ready for the semester
  - New projects in hardware fixes for the shell
  - mounting the apriltags
  - setting up testing
  - Software simulation is a priority
  - Started discussing redesign vs refactor
- Worked on a proposal for ROS to replace the current stack
  - Evaluated the capabilities of the current stack

### Summary

Started off the semester with a meeting to see what projects wee wanted to tackle for the project and the status of the current software.

## 1/17

- Prepared to present to new member presentation
  - Identified ares of need
  - Created possible projects to fit those needs
  - Created a PowerPoint showing past present and future
- Presented and interacted with new members

### Summary

this week was concentrated on creating a presentation for the new members and recruiting.

## 1/24

- Got our first recruits an provided them with docs to read.
- Found a meeting time
- Got the SITL simulation for Ardusub running

### Summary

This week we go the new member results and stated getting them integrated. I also got the SITL simulation for Ardusub.

## 1/31

- First subteam meeting
  - Clarified the purpose of the project
  - went over key problems
  - Talked about our plans for the semester
  - Got some more details on the new members experience and interests
- Setup a Trello
  - Populated with the first set of tasks
  - Assigned category owners
- Started looking into the CAD
  - Looked through the archival versions
  - Installed and setup SOLIDWORKS
- Setup [ignition gazebo](https://ignitionrobotics.org/libs/gazebo)
  - Looking ate this for simulation as a modern replacement for Gazebo
  - setup and got a demo of their underwater simulation running

### Summary

This week We got to some real work starting to create tasks and project management. Additionally started to look into the CAD to determine the extent of the changes that need to be made for easy testing. Then started to work on the simulation environment evaluating ignition gazebo.

## 2/7

- Questions on the stack
  - the new members had some trouble looking through the stack
  - started to think about a rewrite instead of a refactor
  - decided to look into ROS and ROS2
- ROS evaluation
  - Created a [document](https://gtri.box.com/s/omxvszbv9z1tuzo45dlacu8ssr9bsfna) with pros and cons of ROS,ROS2, and current stack
- Started to get the ownership of the Aquabots and VIP-aquabots repos for Dr. West

### Summary

This week was derailed by how hard it was for the new member to get up and running on the current stack this sparked a  discussion which led to us evaluating ROS as an alternative.  

## 2/14

- Decision on rewrite vs refactor
  - Created a poll to get the teams feeling on the question
  - decided to go with ROS2 for the new stack
- Updated the Trello to reflect the pivot
- Tasking for hardware
  - Split up the work and created granular sub tasks for the hardware goals
  - Discussed methods and expectation at the hardware meeting
- Created a proposal for the ROS2 architecture
- Created a new format for presentations so that everyone is participating
- Created a Github organization and skeleton [repos](https://github.gatech.edu/orgs/Aquabots-VIP/teams/roboquarium/repositories) for the project

![architecture](../architecture_proposal/test.dot.svg)

### Summary

This week we decided to start a rewrite with ROS 2 and reworked the tasking to match that. Hardware got started on their projects and we agreed to a more open collaborative presentation style

## 2/21

- Got everyone rolling on ROS 2 installation
- Started to investigate using light houses to solve the 3D localization problem
  - <https://www.bitcraze.io/products/lighthouse-positioning-deck/>
  - <https://github.com/skyviewlabs/vive-diy-position-sensor>
  - <https://hackaday.io/project/19570/logs?sort=oldest>
- Found an implementation of a telemetry bridge for ESP8266 (maybe ESP32 as well)
  - Could be a good replacement for the pi zero w
- started working on cross-compiling ROS2 for the pi zero
  - Got it working in WSL but no in docker.

### Summary

This week is the first week of getting everyone onto ROS 2 and thinking about the research paper. Additionally I worked to get the hardware ready for the software team to get started on.

## 2/28

- Started looking into the ESP8266 link
  - Confirmed it could work with many bots
  - Confirmed that their is an implementation for esp32
- Built a test circuit to try out the firmware
  - Got the circuit working but still need to update the Pixhawk firmware to output telemetry
