# Architecture (ROS 2)

## Group 1 (Teleop) (Jesse, brandon)

### Resposibilities

Create a stack in ROS 2 capable of tele-operation of the bot implement the following:

- [MAVROS](https://github.com/mavlink/mavros/blob/ros2/mavros/README.md) on the pi
- [teleop](https://github.com/ros-teleop/teleop_tools) on the topside computer
- Create a launch file for both bots
- Figure out the networking between the bots.

## Group 2 (Localization) (Nut Shashank )

## Group 3 (Simulation) (locke Armaan)

## Group 4 (control) (Timothy alemu?)

## Group 5 Mechnical (Bubba Anthony Mintesinot)
