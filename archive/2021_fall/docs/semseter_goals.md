# Goals for Fall 2021

## Localization

### Recording Data

- There needs to be way to record data of the pixy cams so that software can develop without needing to run the hardware
- Could be as simple as a CSV file with sequence numbers and a constant dt assumption.
- Additionally there should be a set way to record ground truth for the experiments in the following sections.

### Measuring accuracy/precision of the Pixy cam system

- Measure characteristics of the localization estimate of the pixy cam by generating ground truth data and pixy cam data to compare to. other factors such as mounting position and the use of world coordinates vs image coordinates should be considered.
- I also think we should evaluate other ways of localizing the robots (or at least be ready to pivot depending on the results).

### Building a tracking system

- Build a tracking system for the topside computer to provide the location and/or distance between the agents to the users. This should be based on the generated data from the section above.
- possibly an ensemble Kalman filter or other stochastic tracker. (since we have explicit data association it should be straight forward)

## Basic Formation control

Not really possible to test properly with only 2 bots I think the major question is should the system be bare bones proof of localization or be built to handle N robots and local vs global information.
