# Goals for Fall 2021

## Localization

### Recording Data

- There needs to be way to record data of the pixy cams so that software can develop without needing to run the hardware
- Could be as simple as a CSV file with sequence numbers and a constant dt assumption.
- Additionally there should be a set way to record ground truth for the experiments in the following sections.

### Measuring accuracy/precision of the Pixy cam system

- Measure characteristics of the localization estimate of the pixy cam by generating ground truth data and pixy cam data to compare to. other factors such as mounting position and the use of world coordinates vs image coordinates should be considered.
- I also think we should evaluate other ways of localizing the robots (or at least be ready to pivot depending on the results).

### Building a tracking system

- Build a tracking system for the topside computer to provide the location and/or distance between the agents to the users. This should be based on the generated data from the section above.
- possibly an ensemble Kalman filter or other stochastic tracker. (since we have explicit data association it should be straight forward)

### Basic Formation control

Not really possible to test properly with only 2 bots I think the major question is should the system be bare bones proof of localization or be built to handle N robots and local vs global information.

## Weekly notes

### 9/2/21

### Overall Information

- New lab is in Klaus (kind of small)

#### Roboquarium

- underwater swarm testbed
- Software oriented
- preferred project
- need swarming algorithms!!! (I know how to solve this problem its formations)
- might need to have a sim (maybe even the robotarium) (already in progress)
- control envelope stuff for AUV model much harder (maybe not possible to guarantee in real life)
- Maybe scrimmage?

#### Microfloats

- Swarming microfloats cool communications infrastructure.
- mesh networking??
- uses currents to move around the ocean laterally and longitudinally (altitude is controlled by buoyancy adjustment)
- Need to increase the depth from 450m to 750m
- need to rewrite the software for esp32
- not really for me (needs more mechanical and electrical (send them the grounding guide))

#### Nekton

- Mostly mechanical
- focused on relating underwater exploration and space underwater exploration (looking for life)
- New 3d prints and battery module work maybe an arm and microfloats deployment.
- maybe in a future semester.

#### Prince William Sound

- Measuring the sound reflection using a profiler
- Need a new profiler
- Total redesign of the profiler.
- Ultra wide band for lower power communications perhaps
- plankton camera used to determine if the food sources are recovering

#### Things to do before next meeting

- Need to meet with Michael west about the scope of the project (needs to be larger for masters)
- Contact to ask about joining roboquarium

### 9/8/21

## Schedule and sub-team selection

### Calendar options

- Google calendar added to my personal account
- Not able to edit yet must request from Dr.West
- sub-team meeting Monday 2-3 and 5-6

### Possible testing locations

- CRC pool look in to scheduling.

### 3D printer

- offer my 3D printer for use by the lab.

### Links to resources

- [Github](https://github.com/tom-hightower/SwimmingSwarm)
- [Box](https://gatech.app.box.com/folder/63985263899?s=0xp8sc0mzsax0rd1hbz2h8z2lqvm6kow)

### 9/23/21

Need to speak with team about semester plans and confirm that all of the things we spoke about is helpful

### ID robots

- Aruco/QR/ April tags
- Filter/Hungarian algorithm (no explicit ID)
- PHD tracking (no need for a specific ID)
- Is it even necessary in order to provide local information
  - Do we need to maintain the tracking between time steps.

### PI Troubles

- Static IP and a laptop
- possible I can provide a new wireless router (I have an AP but not a router)

### 3D printing

- I offered my printer to nekton and the other projects JIC.

### 9/27/21

#### Clarifications

- What is the goal of roboquarium?
  - To be deployed in the ocean/river//lake
  - to replicate the robotarium as a testbed
- Is the intention to be able to navigate in full 6DOF in the future?
- Is it going to be important for the bots to communicate?
- Is the goal to scale down the bots even more?

#### Goals

- To evaluate the possible solutions for localization
  - Pixy cam (some inconsistent performance so far)
  - Tag based camera tracking (April tags aruco etc)
  - Color based tracking (might make pixycam an option)
  - Detection and algorithmic ID (arbitrary ID per run)
- Create a tracking system to collate the measurements
  - Ensemble Kalman filter
  - Probability Hypothesis density filter
  - MTT problem has lots of solutions depending on the precision and accuracy of the measurements

### 10/7/21

#### pixycam

- Trying a more matte finish on the tags
- Trying whole object tracking (track the bots as balls)

#### new camera tracking systems

- color based tracking
- shape base tracking
- histogram of oriented gradients

### Simulator

#### gazebo / scrimmage

- Investigate if scrimmage supports underwater vehicles
- What plugins would be required to simulate the roboquarium
- What is the complexity to investigate with HIL systems

#### MATLAB/python

- lookup robotarium sim and see if it could be added to to allow for compatibility
- The physics might be able to be simplified so that Gazebo in necessary.

### 10/14/21

- Tested the stand
  - Worked well ad was easy to setup
  - not super stiff
- Tried to get pixy cam working
  - The color are very de-saturated and it has trouble distinguishing colors even under ideal conditions
  - We tried switching to tracking the whole bot but the results were subpar.
  - In addition with tracking the whole bot we need to split the bot ourselves
- Started work on finding new methods
  - April tags
  - contour tracking
  - shape based

### 10/21/21

- Proposed Tracking methods to test
  - Tags
  - contour tracking single color
  - contour tracking multiple colors
- began development on the contour method
  - implemented using opencv and python
  - produces a bounding box and center

### 10/28/21

- Got position tracking working on old video
  - Got an old video and was able to track a single bot.
- Started to discuss how we could get the orientation
  - finding the line of best fit through the bot
  - Finding a know shape on the bot (arrow) using sift or surf
  - use a compass and align as part of startup

![detection](../img/position.png)

### 11/4/21

- Worked on getting orientation
  - finding the line of best fit through the bot
  - Finding a know shape on the bot (arrow) using sift or surf
  - use a compass and align as part of startup

### 11/11/21

- rewrote the code into a callable structure for the bot
  - created two classes one for detection and one for tracking
  - both should be inter changeable with other methods of detection and tracking
- Wrote a basic track manger that hold the track propagates them based on a nearest neighbor model

- Stated to work on splitting
  - Splitting is the hardest part to scale to N bots
  - The method I use for two bots is to find the centroid of the connected bots and cover the connection point then reevaluate the contours
- Water test and recorded data

![tracking](../img/tracker.png)

### 11/18/21

- implemented orientation and basic splitting
  - orientation is based on the line going through the motor shrouds
  - the orientation is tracked to handle the discontinuities caused by the ambiguity of the line
- implemented fake bots
  - offset the segmented bot the added it back into the original image.

![splitting](../img/splitting.png "Splitting")

### 12/2/21

- implemented tags
- tested tags because I realized a flaw with the contour tracking IDs need to be consistent run to run
- tags do this explicitly and based on testing they are more stable and accurate than the color tracking
- probably should have tried them sooner.

![tags](../img/output.png)
