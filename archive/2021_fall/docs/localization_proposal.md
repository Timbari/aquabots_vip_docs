# Localization Proposal

## Introduction

The problem set out for this semester is the localization on the surface of the water of the prototype aquabots for the roboquarium. The effort will consist of # phases: evaluation of the detection methods (accuracy, precision, detection rate) then based on the data from phase 1 a tracking method can be selected and implemented to match the type quality of the detections. Finally, the integration of the localization system with the current waypoint follower method of control. The previous efforts have focused on the Pixycam which is a self-contained object tracking system.

## Proposed Systems

The theoretical section consist of options for evaluation for both detection and tracking. The systems have been selected based on experience with the method availability of hardware.

### Detection Systems

#### Pixycam

The Pixycam was selected based on the previous usage on the project. The Pixycam is a self contained object detection and tracking system which can track based on color, shape and basic patterns.

**Pros:**

* the camera is integrated i.e. the user does not need to implement the detection or tracking
* The camera is already in use with the bots

**Cons:**

* The detection is sensitive to lighting and reflections
* The range is not certain and requires testing.
* The tracks are in pixel space not real-world units.
* The integrated nature of it makes it more difficult to compensate for the problems with detection.

#### April Tags

The April tag fiducial system is commonly used in motion tracking for land based robots. It combines the identification and localization of a robot into a single tag. The system provides 3D localization allowing for the robots to be easily projected onto a common plane in real would units.

**Pros:**

* The detection system is available with interfaces for python and is easy to use.
* The ID provided makes tracking algorithms simpler to implement since each measurement is automatically associated with the robot state.
* April tags have been tested and characterized.

**Cons:**

* April tags need to be evaluated in the tough reflection and lighting environments provided by the water.
* April tags range and accuracy is tied to the resolution of the camera being used.
* A camera would need to be purchased to detect the tags.

#### Color Based Detection

Using HSV transform it is relatively easy to threshold out colors and using clustering the color blobs can be used as tracking points in the image plane. The robots could be identified by the color of the bot (or a section of the bot) or the colors could be uniform with an algorithm to track the data association. The issue of real world units could be solved with a stereo pair or even more cameras with rigid transforms to provide a full 3D tracking system.

**Pros:**

* The system is easy to build with a relatively small amount of computer vision knowledge
* The system is expandable to multiple cameras for 3D tracking.
* The cameras do not need to be high resolution

**Cons:**

* The purchase of new equipment is necessary
* The most implementation time as the detection system needs to be built from scratch.
* The accuracy and precision need to be tested (no direct prior research)

### Tracking Systems

Once the robots have been detected the system will need to track the robots so that the controls and feedback can be applied consistently.

#### Simple Track Manger

The simplest solution is to take the detections as a rule and map the measurements to the state of the robot. With the small modification of the knowledge of the number and IDs, if applicable, can be know so a missing measurement will not make a robot vanish.

**Pros:**

* The system is very easy to implement
* With know data association it is easy to make sure that tracks persist from frame to frame.

**Cons:**

* The system depends completely on the measurements being correct
* there is no measure of the uncertainty of the system.
* requires data association to function properly.

#### Ensemble Kalman Filter

The ensemble Kalman filter is the linear Kalman filter applied to a Multi-Target Tracking (MTT) problem. The Kalman filter as a stochastic method also for the management of both the tracking state and the uncertainty of the measurements. Data association is also required for the ensemble Kalman filter as each measurement needs to be associated with a filter each frame.

#### Multi-Hypothesis Tracking (MHT)

MHT is a stochastic method of tracking multiple targets without know data association the measurements are all compared to the states and the system is propagated according to a set of Bayesian equations.
