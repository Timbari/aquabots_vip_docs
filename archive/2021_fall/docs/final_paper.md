# Localization Project

## Introduction

The problem set out for this semester is the localization on the surface of the water of the prototype aquabots for the roboquarium. The effort will consist of 2 phases: evaluation of the detection methods (accuracy, precision, detection rate) then based on the data from phase 1 a tracking method can be selected and implemented to match the type quality of the detections. Finally, the integration of the localization system with the current waypoint follower method of control. The previous efforts have focused on the Pixycam which is a self-contained object tracking system.

## Proposed Systems

The theoretical section consist of options for evaluation for both detection and tracking. The systems have been selected based on experience with the method availability of hardware.

### Detection Systems

#### Pixycam

The Pixycam was selected based on the previous usage on the project. The Pixycam is a self contained object detection and tracking system which can track based on color, shape and basic patterns.

**Pros:**

* the camera is integrated i.e. the user does not need to implement the detection or tracking
* The camera is already in use with the bots

**Cons:**

* The detection is sensitive to lighting and reflections
* The range is not certain and requires testing.
* The tracks are in pixel space not real-world units.
* The integrated nature of it makes it more difficult to compensate for the problems with detection.

#### April Tags

The April tag fiducial system is commonly used in motion tracking for land based robots. It combines the identification and localization of a robot into a single tag. The system provides 3D localization allowing for the robots to be easily projected onto a common plane in real would units.

**Pros:**

* The detection system is available with interfaces for python and is easy to use.
* The ID provided makes tracking algorithms simpler to implement since each measurement is automatically associated with the robot state.
* April tags have been tested and characterized.

**Cons:**

* April tags need to be evaluated in the tough reflection and lighting environments provided by the water.
* April tags range and accuracy is tied to the resolution of the camera being used.
* A camera would need to be purchased to detect the tags.

#### Color Based Detection

Using HSV transform it is relatively easy to threshold out colors and using clustering the color blobs can be used as tracking points in the image plane. The robots could be identified by the color of the bot (or a section of the bot) or the colors could be uniform with an algorithm to track the data association. The issue of real world units could be solved with a stereo pair or even more cameras with rigid transforms to provide a full 3D tracking system.

**Pros:**

* The system is easy to build with a relatively small amount of computer vision knowledge
* The system is expandable to multiple cameras for 3D tracking.
* The cameras do not need to be high resolution

**Cons:**

* The purchase of new equipment is necessary
* The most implementation time as the detection system needs to be built from scratch.
* The accuracy and precision need to be tested (no direct prior research)

### Tracking Systems

Once the robots have been detected the system will need to track the robots so that the controls and feedback can be applied consistently.

#### Simple Track Manger

The simplest solution is to take the detections as a rule and map the measurements to the state of the robot. With the small modification of the knowledge of the number and IDs, if applicable, can be know so a missing measurement will not make a robot vanish.

**Pros:**

* The system is very easy to implement
* With know data association it is easy to make sure that tracks persist from frame to frame.

**Cons:**

* The system depends completely on the measurements being correct
* there is no measure of the uncertainty of the system.
* requires data association to function properly.

#### Ensemble Kalman Filter

The ensemble Kalman filter is the linear Kalman filter applied to a Multi-Target Tracking (MTT) problem. The Kalman filter as a stochastic method also for the management of both the tracking state and the uncertainty of the measurements. Data association is also required for the ensemble Kalman filter as each measurement needs to be associated with a filter each frame.

#### Multi-Hypothesis Tracking (MHT)

MHT is a stochastic method of tracking multiple targets without know data association the measurements are all compared to the states and the system is propagated according to a set of Bayesian equations.

## Methodology

This section deals with the method of how the systems will be evaluated. This includes experiments measuring accuracy and precision, ad detection rate of the localization. These test will be run out of the water and in the water. additionally all of the data (video, ground truth, detections) will be recorded so that methods can tested and characterized.

## Systems and Testing

The systems that were tested this semester were contour based detection and April tags. The problems these systems need to solve were the following.

1. Provide an estimate of each bots position in the image frame
2. Transform the Estimate in real space
3. Provide a way to assign each bot a identity
4. Track the estimates and maintain the identity of the robot form frame to frame

### Contour Tracking

In the case of contour tracking the algorithm was:

1. Take in a Image
2. Convert to the HSV color space
3. Segment the image using the hue and saturation
4. Apply morphology to the mask to get rid of small noise and close the shape
5. Find contours in the mask
6. Threshold the contours by size
7. Extract the centroid of the contour and return it as pixel value
8. Scale the value based on the position of the camera

![position](../img/position.png)

This provided good detection of position for one robot. it was relatively simple to extract orientation by the following algorithm

1. fit a line to the extracted contour
2. Calculate the difference between the angle of the line and the last angle
3. find the line perpendicular to that line by setting $\Delta y = \Delta x, \Delta x = -\Delta y$
4. rotate the direction $\pi$ radians if the change is larger that $\pi/2$
5. Store the current angle as the one calculated above

The problem is that the tracking must be perfect is the bots are lost and re-acquired the orientation may be $\pi$ radians off. Additionally this requires state to be kept between frames. The real problem is with scaling when the bots are far enough away splitting is trivial but when a contour encompasses more than one bot the bots need to be split. A method based on adding breaking element such as a line to the image and then re-evaluate. It is possible this could be implemented recursively but it would be slow as it requires the at least N-1 recursions for N bots.

![splitting](../img/splitting.png)

The contour tracking system meets requirements 1,2 adequately If a initialization phase is added where the user assigns an identity to each bot then 3 could be met as well. The problem with splitting is only partially solved so this system would require more work. The consistency of the detections very good qualitatively with the problems being caused by the way the data was recorded

### April Tag Detection

April tags automatically solve 1,2,3 by directly providing a three dimensional pose and an explicit ID. The process getting the pose is simple as one python call on  an image. The disadvantage twofold: the tags need to be integrated into the bots and the detection and pose accuracy are more dependent on the placement and resolution  of the camera and is not as flexible as the color/contour method. However it is by far simpler option and scales better to more bots and the three dimensional case. The testing process was not as rigorous as it should be and is based on work from another project.

![April tags](../img/output.png)

### Tracking

The only tracking system that was implemented this semester was a basic track manger that behaves base on the following algorithm.

1. Initialize with the list of initial detections
2. update the current tracks based on the closest detections
3. If a track is outside the range of any detection do not update

The tracking system was the minimum required for testing.

![tracker](../img/tracker.png)

## Conclusion

This semester I built and tested three methods of tracking the bots for the Roboquarium project. The Pixycam was quickly eliminated based on its poor false positive and false negative rates. The contour based tracking was developed as a low information alternative to the pixy cam. this proved effective for a single bot but scaling was problematic due to the the splitting problem. This was also compounded with the need to add a initialization for permanent identities. The last method was using April tags this explicitly solves the problem of a permanent identity and splitting the bots that are close together. Based on the combination of work done on a GTRI system this semester and testing on scaling down the size of the tags it was established that April tags are the best option for the project going forward for both the current requirements and scaling up numbers of bots and degrees of freedom.

## Appendix

[code development environment](https://gitlab.com/Timbari/bot_traking_roboquaium)
[Pull Request (WIP)](https://github.com/tom-hightower/SwimmingSwarm/pull/15)
[April Tags](https://april.eecs.umich.edu/software/apriltag#:~:text=AprilTag%20is%20a%20visual%20fiducial,tags%20relative%20to%20the%20camera.)
