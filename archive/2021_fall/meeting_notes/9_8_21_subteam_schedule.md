# Schedule and sub-team selection

## Calendar options

- Google calendar added to my personal account
- Not able to edit yet must request from Dr.West
- sub-team meeting Monday 2-3 and 5-6

## Possible testing locations

- CRC pool look in to scheduling.

## 3D printer

- offer my 3D printer for use by the lab.

## Links to resources

- [Github](https://github.com/tom-hightower/SwimmingSwarm)
- [Box](https://gatech.app.box.com/folder/63985263899?s=0xp8sc0mzsax0rd1hbz2h8z2lqvm6kow)
