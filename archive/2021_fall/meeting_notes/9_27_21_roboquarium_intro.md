# Goals and Question s for roboquarium

## Clarifications

- What is the goal of roboquarium?
  - To be deployed in the ocean/river//lake
  - to replicate the robotarium as a testbed
- Is the intention to be able to navigate in full 6DOF in the future?
- Is it going to be important for the bots to communicate?
- Is the goal to scale down the bots even more?

## Goals

- To evaluate the possible solutions for localization
  - Pixy cam (some inconsistent performance so far)
  - Tag based camera tracking (April tags aruco etc)
  - Color based tracking (might make pixycam an option)
  - Detection and algorithmic ID (arbitrary ID per run)
- Create a tracking system to collate the measurements
  - Ensemble Kalman filter
  - Probability Hypothesis density filter
  - MTT problem has lots of solutions depending on the precision and accuracy of the measurements
