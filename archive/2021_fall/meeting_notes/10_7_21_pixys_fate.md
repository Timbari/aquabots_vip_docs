# Meeting notes

## Localization

### pixycam

* Trying a more mate finish on the tags
* Trying whole object tracking (track the bots as balls)

### new camera tracking systems

* color based tracking
* shape base tracking
* histogram of oriented gradients

## Simulator

### gazebo / scrimmage

* Investigate if scrimmage supports underwater vehicles
* What plugins would be required to simulate the roboquarium
* What is the complexity to investigate with HIL systems 

### MATLAB/python

* lookup robotarium sim and see if it could be added to to allow for compatibility
* The physics might be able to be simplified so that Gazebo in necessary.
