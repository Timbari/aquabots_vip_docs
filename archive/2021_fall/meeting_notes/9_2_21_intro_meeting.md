# Introductory meeting

## Overall Information

- New lab is in Klaus (kind of small)

## Project descriptions

### Roboquarium

- underwater swarm testbed
- Software oriented
- preferred project
- need swarming algorithms!!! (I know how to solve this problem its formations)
- might need to have a sim (maybe even the robotarium) (already in progress)
- control envelope stuff for AUV model much harder (maybe not possible to guarantee in real life)
- Maybe scrimmage?

### Microfloats

- Swarming microfloats cool communications infrastructure.
- mesh networking??
- uses currents to move around the ocean laterally and longitudinally (altitude is controlled by buoyancy adjustment)
- Need to increase the depth from 450m to 750m
- need to rewrite the software for esp32
- not really for me (needs more mechanical and electrical (send them the grounding guide))

### Nekton

- Mostly mechanical
- focused on relating underwater exploration and space underwater exploration (looking for life)
- New 3d prints and battery module work maybe an arm and microfloats deployment.
- maybe in a future semester.

### Prince William Sound

- Measuring the sound reflection using a profiler
- Need a new profiler
- Total redesign of the profiler.
- Ultra wide band for lower power communications perhaps
- plankton camera used to determine if the food sources are recovering

## Things to do before next meeting

- Need to meet with Michael west about the scope of the project (needs to be larger for masters)
- Contact to ask about joining roboquarium
